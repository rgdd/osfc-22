# README

This is my [open source firmware conference][] talk, 2022.  The used beamer
template is available in the Sigsum project's [documentation repo][].

[open source firmware conference]: https://www.osfc.io/
[documentation repo]: https://gitlab.sigsum.org/sigsum/project/documentation/-/tree/main/beamer
